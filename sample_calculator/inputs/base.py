# SPDX-FileCopyrightText: 2020 German Aerospace Center (DLR)
# SPDX-License-Identifier: MIT


"""
Provides basic framework to read sample values from various sources.
This is referred to as InputMethod.

Notes on adding a new input method:

* Create a new module in sample_calculator.inputs.inputs (e.g., database_input).
* Define a string constant which uniquely identifies the new input method (e.g., DATABASE_INPUT = "DATABASE")
* Make the name constant available outside the package (see __init__.py for details)
* Subclass :class:`InputMethod` (e.g., DatabaseInputMethod)

 * Define the name using the string constant
 * You can provide a custom constructor which can take any parameters
 * Indicate errors using :class:`InputMethodError`

* Always use the factory function :func:`create_inputmethod` to obtain a specific instance
* The new input method method is automatically registered in the factory method
* The new calculation is automatically registered in the factory method

"""


import abc
import inspect
import pkgutil

from sample_calculator.inputs import inputs

_INPUT_METHOD_CACHE = dict()


def create_inputmethod(inputmethod_name, *args, **kwargs):
    """ Returns the InputMethod configured with
    the provided arguments.

    :param inputmethod_name: Name of the InputMethod.
    :param args: Additional arguments of the InputMethod.
    :param kwargs: Additional keyword arguments of the InputMethod.

    :returns: The configured InputMethod.

    :raises: :class:`InputMethodError`
    """

    # Initialize cache
    if not _INPUT_METHOD_CACHE:
        for module_importer, fullname, _ in pkgutil.walk_packages(inputs.__path__, inputs.__name__ + "."):
            module_loader = module_importer.find_module(fullname)
            module = module_loader.load_module(fullname)
            for _, classobj in inspect.getmembers(module):
                try:
                    if InputMethod in classobj.mro():
                        _INPUT_METHOD_CACHE[classobj.name] = classobj
                        break
                except AttributeError:
                    pass  # Simply ignoring non-class members

    # Returning requested input method
    if inputmethod_name in _INPUT_METHOD_CACHE:
        try:
            return _INPUT_METHOD_CACHE[inputmethod_name](*args, **kwargs)
        except TypeError as error:
            raise InputMethodError("Invalid arguments provided -- '{0}'".format(error.args))
    else:
        raise InputMethodError("The input method '{0}' does not exist.".format(inputmethod_name))


class InputMethodError(Exception):
    """ Indicates errors during input retrieval. """


class InputMethod(metaclass=abc.ABCMeta):
    """ Describes the InputMethod interface. """

    @abc.abstractproperty
    def name(self):
        """ Identifies uniquely the InputMethod. """

    @abc.abstractmethod
    def get_input_values(self):
        """ Returns the input values.

        :returns: Sequence of numeric values.

        :raises: :class:`InputMethodError`
        """
